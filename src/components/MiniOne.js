import React from 'react'
import {Button} from 'react-bootstrap'

export default class MiniOne extends React.Component {
  constructor(props){
    super(props)
    this.state = {number: props.number}
  }

  miniOne(){
    this.setState({number: this.state.number-1})
  }

  render() {
    return (
        <div style={{color:'#ff3300', fontSize: "30px"}}>
          {this.state.number}
          <br/>
          <Button bsStyle="danger" style={{color:'#990099'}} onClick={this.miniOne.bind(this)}>Mini 1</Button>
        </div>
    )
  }
}
