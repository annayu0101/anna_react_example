import React from 'react'
import {Button} from 'react-bootstrap'

export default class MluTwo extends React.Component {
  constructor(props) {
    super(props)
    this.state = {number: props.number}
  }

  mulTwo() {

    this.setState({number: this.state.number*2})

  }


  render() {
    return (
        <div style={{color:'#00cc00', fontSize: "30px"}}>
          {this.state.number}
          <br/>
          <Button bsStyle="success" style={{color:'#990099'}} onClick={this.mulTwo.bind(this)}>Mul 2</Button>
        </div>
    )
  }
}
