import React from 'react'
import {Button} from 'react-bootstrap'

export default class PlusOne extends React.Component {
  constructor(props) {
    super(props)
    this.state = {number: props.number}
  }

  addOne() {
    // let number = this.state.number
    // number.push(this.state.number+1)矩正才用push
    this.setState({number: this.state.number+1})
  }


  render() {
    return (
        <div style={{color:'#0066ff', fontSize: "30px"}}>
          {this.state.number}
          <br/>
          <Button bsStyle="info" style={{color:'#990099'}} onClick={this.addOne.bind(this)}>Add 1</Button>
        </div>
    )
  }
}

//
