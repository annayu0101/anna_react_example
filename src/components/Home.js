import React from 'react'
import {Button} from 'react-bootstrap'

import MiniOne from './MiniOne.js'
import MulTwo from './MulTwo.js'
import PlusOne from './PlusOne.js'
import DivTwo from './DivTwo.js'

export default class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {number1: 0, number2: 0, number3: 1, number4: 100}
  }

  render() {
    return (
        <div>
          {this.props.text}
          <PlusOne number={this.state.number1} />
          <MiniOne number={this.state.number2} />
          <MulTwo number={this.state.number3} />
          <DivTwo number={this.state.number4} />
        </div>
    )
  }
}
